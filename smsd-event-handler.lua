local _M = {}

local smsd_helpers = require("smsd-helpers")
local handler_helpers = require("handler-helpers")
local lfs = require("lfs")

function _M.process_received(message, is_authorized, forward_phone, outgoing_dir)
	if (is_authorized) then
		-- call submodule and sent back its reply
		smsd_helpers.send_message(message["From"], message["Message_id"], handler_helpers.do_module_process(message["Message"], "index", "sms-processors.", message), outgoing_dir)
	else
		-- forward unauthorized
		smsd_helpers.send_message(forward_phone, message["Message_id"], "Fwd +" .. message["From"] .. ": " .. message["Message"], outgoing_dir)
	end
end

function _M.process_sent(msg_file, sent_msg_id)
	if (sent_msg_id ~= nil) then
		-- if the message should be reported then label the message file with the message's ID
		assert(os.rename(msg_file, msg_file .. "." .. sent_msg_id))
	end
end

function _M.process_report(msg_file, message, sent_dir)
	local sent_msg_id = tostring(message["REPORT_Message_id"])
	local msg_file_dir = msg_file:gmatch("(.*)/[^/]+$")()
	for file in lfs.dir(sent_dir) do
		-- find a matching sent file
		if (file:sub(-1 * sent_msg_id:len() - 1) == "." .. sent_msg_id) then
			-- rename the report file to the sent file's name (lfs.dir item is always basename, i.e., without a path)
			local msg_file_new = msg_file_dir .. "/" .. file
			local sent_file = sent_dir .. "/" .. file
			assert(os.rename(msg_file, msg_file_new))
			-- append the sent file to the report file
			local file_in = assert(io.open(sent_file, "r"))
			local file_out = assert(io.open(msg_file_new, "a"))
			io.output(file_out)
			while true do
				local line = file_in:read("*l")
				if (line == nil) then
					break
				else
					io.write(line .. "\n")
				end
			end
			io.close(file_out)
			io.close(file_in)
			-- remove the sent file
			assert(os.remove(sent_file))
			-- exit
			break
		end
	end
end

function _M.process_call(message, is_authorized, forward_phone, outgoing_dir)
	if (is_authorized) then
		-- call submodule and sent back its reply
		smsd_helpers.send_message(message["From"], nil, handler_helpers.do_module_process("call_" .. message["From"], "index", "sms-processors.", message), outgoing_dir)
	else
		-- forward unauthorized
		smsd_helpers.send_message(forward_phone, message["Message_id"], "Call +" .. message["From"] .. ": " .. message["Call_type"] .. " " .. message["Received"], outgoing_dir)
	end
end

function _M.print_help(arg)
	io.stderr:write("Usage: " .. arg[0] .. " <RECEIVED|SENT|REPORT|CALL|FAILED> <MSGFILE> [SENT_MSG_ID]\n")
	io.stderr:write("Process SMS messages produced by smsd in smstools3.\n")
end

function _M.main(arg, whitelist_file, outgoing_dir, sent_dir, admin)
	if (#arg < 2 or arg[#arg] == "--help") then
		_M.print_help(arg)
		return 1
	end

	local action = arg[1]
	local msg_file = arg[2]
	local sent_msg_id = arg[3]

	if (action == "RECEIVED") then
		local message = assert(smsd_helpers.parse_msg_file(msg_file))
		local whitelist = assert(smsd_helpers.parse_numberlist_file(whitelist_file))
		local is_authorized = whitelist[message["From"]] ~= nil
		_M.process_received(message, is_authorized, admin, outgoing_dir)
	elseif (action == "SENT") then
		_M.process_sent(msg_file, sent_msg_id)
	elseif (action == "REPORT") then
		local message = assert(smsd_helpers.parse_msg_file(msg_file))
		_M.process_report(msg_file, message, sent_dir)
	elseif (action == "CALL") then
		local message = assert(smsd_helpers.parse_msg_file(msg_file))
		local whitelist = assert(smsd_helpers.parse_numberlist_file(whitelist_file))
		local is_authorized = whitelist[message["From"]] ~= nil
		_M.process_call(message, is_authorized, admin, outgoing_dir)
	elseif (action == "FAIL") then
		-- NOP
	else
		_M.print_help(arg)
		return 2
	end

	return 0
end

return _M
