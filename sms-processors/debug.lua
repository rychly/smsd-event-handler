local _M = {}

function _M.process(cmd, message)
	local result = ""
	for key, value in pairs(message) do
		result = result .. key .. ": " .. value .. "\n"
	end
	return result
end

return _M
