#!/bin/sh

UCI_DIR=/etc/config

if ! [ -d "${UCI_DIR}" ]; then
	mkdir -p ${UCI_DIR}
	chmod 1777 ${UCI_DIR}
fi

# -- Initialize UCI config

touch ${UCI_DIR}/lua

uci -c ${UCI_DIR} set lua.sms=lua
uci -c ${UCI_DIR} set lua.sms.whitelist_file='/etc/smsd.white'
uci -c ${UCI_DIR} set lua.sms.outgoing_dir='/tmp/sms/outgoing'
uci -c ${UCI_DIR} set lua.sms.admin_phone='420123456789'
uci -c ${UCI_DIR} commit
